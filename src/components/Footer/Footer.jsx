import React from 'react';

export const Footer = () => {
  return (
    <footer className="footer content">
      <p className="footer_title">Contacts</p>
      <div className="footer_content">
        <form action="" className="footer_form">
          <input type="text" placeholder="Your name:" className="footer_input" />
          <input type="text" placeholder="Your email:" className="footer_input" />
          <textarea
            name=""
            id=""
            cols="30"
            rows="10"
            placeholder="Your message:"
            className="footer_input"
          />
          <input type="button" value="SEND" className="form_button" />
        </form>
        <div className="feedback">
          <div className="skype social">
            <img src="img/skype.png" alt="skype" />
            <p className="social_description">here_your_login_skype</p>
          </div>
          <div className="icq social">
            <img src="img/icq.png" alt="skype" />
            <p className="social_description">279679659</p>
          </div>
          <div className="email social">
            <img src="img/email.png" alt="skype" />
            <p className="social_description">psdhtmlcss@mail.ru</p>
          </div>
          <div className="phone social">
            <img src="img/phone.png" alt="skype" />
            <p className="social_description">80 00 4568 55 55</p>
          </div>
          <img src="img/social.png" alt="skype" className="social_icons" />
        </div>
      </div>
    </footer>
  );
};
