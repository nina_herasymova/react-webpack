import React from 'react';

export const ReviewItem = (props) => {
  return (
    <div className="review_item">
      <div className="review_image" />
      <p className="left" />
      <div className="review_description">
        <div>
          <i>{props.text}</i>
        </div>
        <span className="author">{props.author}</span>
      </div>
    </div>
  );
};
