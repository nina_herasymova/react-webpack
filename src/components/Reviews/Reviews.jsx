import React from 'react';
import { ReviewItem } from './ReviewItem';

export const Reviews = () => {
  return (
    <section className="review content">
      <p className="review title">Reviews</p>
      <div className="review_items">
        <ReviewItem
          author="Donec P."
          text="Integer enim neque volutpat ac tincidunt volutpat diam
           ut venenatis tellus in metus vulputate eu scelerisque felis
            imperdiet proin fermentum leo vel orci porta"
        />
        <ReviewItem
          author="Lourens S."
          text="Ultricies leo integer malesuada nunc vel risus commodo
           viveccumsan lacus vel facilisis volutpat est velit egestas dui
            id ornare arcu odio ut sem nulla pharetra diam sit amet"
        />
        <ReviewItem
          author="Egestas T."
          text="Interdum velit laoreet id donec ultrdales ut etiam sit
          amet nisl purus in mollis nunc sed id semper risus in hendrerit
           gravida rutrum quisque"
        />
        <ReviewItem
          author="Mollis R."
          text="Est placerat in egestas erat imperdiet m ut porttitor leo
          a diam sollicitudin tempor id eu nisl nunc mi ipsum faucibus vitae aliquet nec"
        />
      </div>
    </section>
  );
};
