import React from 'react';

export const HeaderItem = (props) => {
  return (
    <div className="header_item">
      <img src="img/check.png" alt="check" className="check" />
      <p>{props.text}</p>
    </div>
  );
};
