import React from 'react';
import { HeaderItem } from './HeaderItem';

export const Header = () => {
  return (
    <header className="header content">
      <div className="header_description">
        <p className="header_title">Product name</p>
        <HeaderItem text="Put on this page information about your product" />
        <HeaderItem text="A detailed description of your product" />
        <HeaderItem text="Tell us about the advantages and merits" />
        <HeaderItem text="Associate the page with the payment system" />
      </div>
      <div className="header_image" />
    </header>
  );
};
