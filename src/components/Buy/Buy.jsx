import React from 'react';

export const Buy = () => {
  return (
    <section className="buy content">
      <p className="buy title">Buy it now</p>
      <div className="buy_items">
        <div className="buy_item">
          <p className="buy_item_title">Standart</p>
          <p className="buy_item_price">$100</p>
          <ol className="buy_item_list">
            <li>Porro officia cumque sint deleniti;</li>
            <li>Тemo facere rem vitae odit;</li>
            <li>Cum odio, iste quia doloribus autem;</li>
            <li>Aperiam nulla ea neque</li>
          </ol>
          <input type="button" value="BUY" className="buy_button" />
        </div>
        <div className="buy_item">
          <p className="buy_item_title">Premium</p>
          <p className="buy_item_price">$150</p>
          <ol className="buy_item_list">
            <li>Porro officia cumque sint deleniti;</li>
            <li>Тemo facere rem vitae odit;</li>
            <li>Cum odio, iste quia doloribus autem;</li>
            <li>Aperiam nulla ea neque</li>
            <li>Porro officia cumque sint deleniti;</li>
            <li>Тemo facere rem vitae odit;</li>
            <li>Cum odio, iste quia doloribus autem;</li>
            <li>Aperiam nulla ea neque</li>
          </ol>
          <input type="button" value="BUY" className="buy_button" />
        </div>
        <div className="buy_item">
          <p className="buy_item_title">Lux</p>
          <p className="buy_item_price">$200</p>
          <ol className="buy_item_list">
            <li>Porro officia cumque sint deleniti;</li>
            <li>Тemo facere rem vitae odit;</li>
            <li>Cum odio, iste quia doloribus autem;</li>
            <li>Aperiam nulla ea neque</li>
            <li>Porro officia cumque sint deleniti;</li>
            <li>Тemo facere rem vitae odit;</li>
            <li>Cum odio, iste quia doloribus autem;</li>
            <li>Aperiam nulla ea neque</li>
            <li>Porro officia cumque sint deleniti;</li>
            <li>Тemo facere rem vitae odit;</li>
            <li>Cum odio, iste quia doloribus autem;</li>
            <li>Aperiam nulla ea neque</li>
          </ol>
          <input type="button" value="BUY" className="buy_button" />
        </div>
      </div>
    </section>
  );
};
