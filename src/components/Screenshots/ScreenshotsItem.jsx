import React from 'react';

export const ScreenshotsItem = (props) => {
  return (
    <div className="screenshots_item">
      <div className="screenshots_image" />
      <div className="screenshots_description">
        <p className="screenshots_description_title"> The description for the image</p>
        <p>{props.text}</p>
      </div>
    </div>
  );
};
