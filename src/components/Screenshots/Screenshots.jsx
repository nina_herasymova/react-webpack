import React from 'react';
import { ScreenshotsItem } from './ScreenshotsItem';

export const Screenshots = () => {
  return (
    <section className="screenshots content">
      <p className="screenshots title">Scrinshots</p>
      <div className="screenshots_items">
        <ScreenshotsItem
          text="integer enim neque volutpat ac tincidunt volutpat
        diam ut venenatis c tincidunt volutpat diam ut venetellus in metus vulputate
         eu scelerisque felis imperdiet proin fermentum leo vel orci porta"
        />
        <ScreenshotsItem
          text="ultricies leo integer malesuada nunc vel risus commodo
         viveccumsan lacus vel facilisc tincidunt volutpat diam ut veneis volutpat
          est velit egestas dui id ornare arcu odio ut sem nulla pharetra diam sit amet"
        />
        <ScreenshotsItem
          text="interdum velit laoreet id donec ultrdalesc tincidunt volutpat
          diam ut vene ut etiam sit amet nisl purus in mollis nunc sed id
           semper risus in hendrerit gravida rutrum quisque"
        />
        <ScreenshotsItem
          text="est placerat in egestas erat imperdiet m
        ut porttitor leo a diam sollicitc tincidunt volutpat diam ut veneudin
        tempor id eu nisl nunc mi ipsum faucibus vitae aliquet nec"
        />
      </div>
    </section>
  );
};
