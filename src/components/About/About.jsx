import React from 'react';

export const About = () => {
  return (
    <section className="about content">
      <div className="about_description">
        <p className="about title">About your product</p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga, illo at.
          Natus eos, eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam
          sapiente ut ipsum.
        </p>
        <p>
          Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem
          ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde
          perferendis nemo debitis dolor.
        </p>
      </div>
      <div className="about_image" />
    </section>
  );
};
