import React from 'react';

export const DignityItem = (props) => {
  return (
    <div className="dignity_item">
      <img src="img/plus.png" alt="plus" className="plus" />
      <div className="dignity_description">{props.message}</div>
    </div>
  );
};
