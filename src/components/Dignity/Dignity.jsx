import React from 'react';
import { DignityItem } from './DignityItem';

export const Dignity = () => {
  return (
    <section className="dignity content">
      <p className="dignity title">Dignity and pluses product</p>
      <div className="dignity_items">
        <DignityItem
          message="Dnteger enim neque volutpat ac tinciduntultricies
        leo integer t sem nulla pharetra diam sit ametultricies leo integer
        t sem nulla pharetra diam sit amet v o vel orci porta"
        />
        <DignityItem
          message="Ultricies leo integer t sem nullaultricies
        leo integer t sem nulla pharetra diam sit ametultricies leo integer
        t sem nulla pharetra diam sit amet pharetra diam sit amet"
        />
        <DignityItem
          message="Pnterdum velitid semper risus in hendrleo integer
         t sem nullaultricies leo integer t sem nulla pharetra dultricies
         leo integer t sem nulla pharetra diam sit ameterit gravida rutrum quisque"
        />
        <DignityItem
          message="Rst pempor id eu nisl nunc mi ipsum faucleo integer
        t sem nullaultricies leo integer t sem nulla pharetra dibus vitae aliquet nec"
        />
        <DignityItem
          message="Kliquet porttitomet justo donec enim dileo integer t
         sem nullaultricies leo integer t sem nulla pharetra dam vulputate ut"
        />
        <DignityItem
          message="Vullam non nisi est sit amet dui nunc mleo integer t
         sem nullaultricies leo integer t sem nulla pharetra dattis enim ut tellus elementum"
        />
      </div>
    </section>
  );
};
