import React from 'react';

import { Header } from './components/Header/Header';
import { About } from './components/About/About';
import { Dignity } from './components/Dignity/Dignity';
import { Screenshots } from './components/Screenshots/Screenshots';
import { Reviews } from './components/Reviews/Reviews';
import { Buy } from './components/Buy/Buy';
import { Footer } from './components/Footer/Footer';

export const App = () => {
  return (
    <div className="App">
      <Header />
      <About />
      <Dignity />
      <Screenshots />
      <Reviews />
      <Buy />
      <Footer />
    </div>
  );
};
